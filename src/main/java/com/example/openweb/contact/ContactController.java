package com.example.openweb.contact;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import com.example.openweb.skill.Skill;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.hateoas.CollectionModel;
import org.springframework.hateoas.EntityModel;
import org.springframework.hateoas.IanaLinkRelations;
import org.springframework.hateoas.MediaTypes;
import org.springframework.hateoas.mediatype.problem.Problem;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.linkTo;
import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.methodOn;

@RestController
@Api("Contact API which controls all CRUD operations for contacts as well as adding and deleting skills related to them")
class ContactController {

    private final ContactRepository repository;
    private final ContactModelAssembler assembler;
    private final ContactVerification verification = new ContactVerification();

    ContactController(ContactRepository repository, ContactModelAssembler assembler) {
        this.repository = repository;
        this.assembler = assembler;
    }

    @GetMapping("/contacts")
    @ApiOperation(value = "Gets all the contacts from the database", response = Contact.class, responseContainer = "CollectionModel")
    CollectionModel<EntityModel<Contact>> all() {
        List<EntityModel<Contact>> contacts = repository.findAll().stream() //
                .map(assembler::toModel) //
                .collect(Collectors.toList());

        return CollectionModel.of(contacts, linkTo(methodOn(ContactController.class).all()).withSelfRel());
    }

    @GetMapping("/contacts/{id}")
    @ApiOperation(value = "Gets a specific contact from the database by id", response = Contact.class)
    EntityModel<Contact> one(@PathVariable Long id) {

        Contact contact = repository.findById(id) //
                .orElseThrow(() -> new ContactNotFoundException(id));

        return assembler.toModel(contact);
    }

    @PostMapping("/contacts")
    @ApiOperation(value = "Adds a contact to the database")
    ResponseEntity<?> newContact(@RequestBody Contact newContact) {
        System.out.println(newContact.toString());
        Map<Boolean,String> response = verification.checkContact(newContact);
        if(response.get(true) != null){
                return ResponseEntity //
                        .status(HttpStatus.METHOD_NOT_ALLOWED) //
                        .header(HttpHeaders.CONTENT_TYPE, MediaTypes.HTTP_PROBLEM_DETAILS_JSON_VALUE) //
                        .body(Problem.create() //
                                .withTitle("Method not allowed") //
                                .withDetail(response.get(true)));
        }
        newContact.setFullName(newContact.getFirstName() + " " + newContact.getLastName());
        EntityModel<Contact> entityModel = assembler.toModel(repository.save(newContact));

        return ResponseEntity //
                .created(entityModel.getRequiredLink(IanaLinkRelations.SELF).toUri()) //
                .body(entityModel);
    }

    @PutMapping("/contacts/{id}")
    @ApiOperation(value = "Modifies a contact from the database by id")
    ResponseEntity<?> replaceContact(@RequestBody Contact newContact, @PathVariable Long id) {
        Map<Boolean,String> response = verification.checkContact(newContact);
        if(response.get(true) != null){
            return ResponseEntity //
                    .status(HttpStatus.METHOD_NOT_ALLOWED) //
                    .header(HttpHeaders.CONTENT_TYPE, MediaTypes.HTTP_PROBLEM_DETAILS_JSON_VALUE) //
                    .body(Problem.create() //
                            .withTitle("Method not allowed") //
                            .withDetail(response.get(true)));
        }
        Contact updatedContact = repository.findById(id) //
                .map(contact -> {
                    contact.setId(id);
                    contact.setFirstName(newContact.getFirstName());
                    contact.setLastName(newContact.getLastName());
                    contact.setPhoneNumber(newContact.getPhoneNumber());
                    contact.setAddress(newContact.getAddress());
                    contact.setEmail(newContact.getEmail());
                    contact.setFullName(newContact.getFirstName() + " " + newContact.getLastName());
                    return repository.save(contact);
                }) //
                .orElseGet(() -> {
                    newContact.setId(id);
                    return repository.save(newContact);
                });

        EntityModel<Contact> entityModel = assembler.toModel(updatedContact);

        return ResponseEntity //
                .created(entityModel.getRequiredLink(IanaLinkRelations.SELF).toUri()) //
                .body(entityModel);
    }

    @DeleteMapping("/contacts/{id}")
    @ApiOperation(value = "Deletes a contact from the database by id")
    ResponseEntity<?> deleteContacts(@PathVariable Long id) {
        Contact contact = repository.getOne(id);
        contact.removeSkills();
        repository.save(contact);
        repository.deleteById(id);

        return ResponseEntity.noContent().build();
    }

    @PostMapping("/contacts/deleteSkill/{id}")
    @ApiOperation(value = "Deletes a skill from the contact the database by id and skill")
    ResponseEntity<?> deleteSkill(@PathVariable Long id, @RequestBody Skill skill) {

        Contact contact = repository.findById(id).orElseThrow(() -> new ContactNotFoundException(id));
        contact.removeSkill(skill);
        repository.save(contact);
        System.out.println(contact.getskill());
        EntityModel<Contact> entityModel = assembler.toModel(contact);
        return ResponseEntity //
                .created(entityModel.getRequiredLink(IanaLinkRelations.SELF).toUri()) //
                .body(entityModel);
    }

    @PostMapping("/contacts/addSkill/{id}")
    @ApiOperation(value = "Adds a skill from the contact the database by id and skill")
    ResponseEntity<?> addSkill(@PathVariable Long id, @RequestBody Skill skill) {

        Contact contact = repository.findById(id).orElseThrow(() -> new ContactNotFoundException(id));
        Skill skill1 = new Skill(skill.getName(), skill.getLevel());
        contact.getskill().add(skill1);
        repository.save(contact);
        EntityModel<Contact> entityModel = assembler.toModel(contact);

        return ResponseEntity //
                .created(entityModel.getRequiredLink(IanaLinkRelations.SELF).toUri()) //
                .body(entityModel);
    }
}