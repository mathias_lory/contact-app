package com.example.openweb.contact;

import com.example.openweb.util.Verification;

import java.util.HashMap;
import java.util.Map;

public class ContactVerification extends Verification {

    public Map<Boolean, String> checkContact(Contact newContact) {
        Map<Boolean, String> responseMap = new HashMap<>();
        checkOnlyChar(newContact.getFirstName(), "firstname", responseMap);
        checkOnlyChar(newContact.getLastName(), "lastName", responseMap);
        checkOnlyNum(newContact.getPhoneNumber(), responseMap);
        checkMail(newContact.getEmail(), responseMap);
        checkAlphaNum(newContact.getAddress(), "lastName", responseMap);
        return responseMap;
    }



}
