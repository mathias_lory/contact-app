package com.example.openweb.skill;

public enum Level {
    FUNDAMENTAL,
    NOVICE,
    INTERMEDIATE,
    ADVANCED,
    EXPERT
}
