package com.example.openweb.skill;

import com.example.openweb.contact.Contact;
import com.fasterxml.jackson.annotation.*;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

@Entity
@JsonIdentityInfo(generator = ObjectIdGenerators.PropertyGenerator.class,property = "id")
public
class Skill {

    private @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "skill_sequence")
    @SequenceGenerator(name = "skill_sequence", sequenceName = "skill_sequence")
    Long id;
    private String name;
    private Level level;
    @ManyToMany(targetEntity = Contact.class,cascade = CascadeType.ALL )
    @JsonIgnore
    private List<Contact> contact;

    public Skill() {
    }

    public Skill(String name, Level level) {
        this.name = name;
        this.level = level;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Level getLevel() {
        return level;
    }

    public void setLevel(Level level) {
        this.level = level;
    }

    public List<Contact> getContact() {
        if(contact == null){
            contact = new ArrayList<>();
        }
        return contact;
    }

    public void setContact(List<Contact> contact) { this.contact = contact; }

    public void addContact(Contact contact) {
        this.contact.add(contact);
    }

    public void removeContact(Contact contact) {
        this.contact.remove(contact);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Skill skills = (Skill) o;
        return Objects.equals(id, skills.id) && Objects.equals(name, skills.name) && Objects.equals(level, skills.level);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, name, level);
    }

    @Override
    public String toString() {
        return "Skills{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", level='" + level + '\'' +
                '}';
    }
}