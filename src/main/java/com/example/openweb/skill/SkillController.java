package com.example.openweb.skill;

import com.example.openweb.contact.Contact;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.hateoas.CollectionModel;
import org.springframework.hateoas.EntityModel;
import org.springframework.hateoas.IanaLinkRelations;
import org.springframework.hateoas.MediaTypes;
import org.springframework.hateoas.mediatype.problem.Problem;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.linkTo;
import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.methodOn;

@RestController
@Api("Skill API which controls all CRUD operations for skills as well as adding and deleting contacts related to them")
class SkillController {

    private final SkillRepository skillRepository;
    private final SkillModelAssembler assembler;
    private final SkillVerification verification = new SkillVerification();

    SkillController(SkillRepository skillRepository, SkillModelAssembler assembler) {

        this.skillRepository = skillRepository;
        this.assembler = assembler;
    }

    @GetMapping("/skills")
    @ApiOperation(value = "Gets all skills from the database", response = Skill.class, responseContainer = "CollectionModel")
    CollectionModel<EntityModel<Skill>> all() {

        List<EntityModel<Skill>> skills = skillRepository.findAll().stream() //
                .map(assembler::toModel) //
                .collect(Collectors.toList());

        return CollectionModel.of(skills, //
                linkTo(methodOn(SkillController.class).all()).withSelfRel());
    }

    @GetMapping("/skills/{id}")
    @ApiOperation(value = "Gets a skill from the database by id", response = Skill.class)
    EntityModel<Skill> one(@PathVariable Long id) {

        Skill skill = skillRepository.findById(id) //
                .orElseThrow(() -> new SkillNotFoundException(id));

        return assembler.toModel(skill);
    }

    @PostMapping("/skills")
    @ApiOperation(value = "Adds a skill to the database")
    ResponseEntity<?> newSkill(@RequestBody Skill skill) {
        Map<Boolean,String> response = verification.checkContact(skill);
        if(response.get(true) != null){
            return ResponseEntity //
                    .status(HttpStatus.METHOD_NOT_ALLOWED) //
                    .header(HttpHeaders.CONTENT_TYPE, MediaTypes.HTTP_PROBLEM_DETAILS_JSON_VALUE) //
                    .body(Problem.create() //
                            .withTitle("Method not allowed") //
                            .withDetail(response.get(true)));
        }

        Skill newSkill = skillRepository.save(skill);

        return ResponseEntity //
                .created(linkTo(methodOn(SkillController.class).one(newSkill.getId())).toUri()) //
                .body(assembler.toModel(newSkill));
    }

    @PutMapping("/skills/{id}")
    @ApiOperation(value = "Modifies a skill from the database by id")
    ResponseEntity<?> replaceSkill(@RequestBody Skill newSkill, @PathVariable Long id) {
        Map<Boolean,String> response = verification.checkContact(newSkill);
        if(response.get(true) != null){
            return ResponseEntity //
                    .status(HttpStatus.METHOD_NOT_ALLOWED) //
                    .header(HttpHeaders.CONTENT_TYPE, MediaTypes.HTTP_PROBLEM_DETAILS_JSON_VALUE) //
                    .body(Problem.create() //
                            .withTitle("Method not allowed") //
                            .withDetail(response.get(true)));
        }
        Skill updatedSkill = skillRepository.findById(id) //
                .map(skill -> {
                    skill.setId(id);
                    skill.setName(newSkill.getName());
                    skill.setLevel(newSkill.getLevel());
                    return skillRepository.save(skill);
                }) //
                .orElseGet(() -> {
                    newSkill.setId(id);
                    return skillRepository.save(newSkill);
                });

        EntityModel<Skill> entityModel = assembler.toModel(updatedSkill);

        return ResponseEntity //
                .created(entityModel.getRequiredLink(IanaLinkRelations.SELF).toUri()) //
                .body(entityModel);
    }

    @DeleteMapping("/skills/{id}")
    @ApiOperation(value = "Deletes a skill from the database by id")
    ResponseEntity<?> deleteSkills(@PathVariable Long id) {
        Skill skill = skillRepository.getOne(id);
        List<Contact> contactList = skill.getContact();
        for(Contact contact: contactList){
            skill.removeContact(contact);
        }
        skillRepository.deleteById(id);

        return ResponseEntity.noContent().build();
    }

    @PostMapping("/skills/deleteContact/{id}")
    @ApiOperation(value = "Deletes a contact from a skill from the database by id and contact")
    ResponseEntity<?> deleteContact(@PathVariable Long id, @RequestBody Contact contact) {

        Skill Skill = skillRepository.findById(id).orElseThrow(() -> new SkillNotFoundException(id));
        Skill.removeContact(contact);
        EntityModel<Skill> entityModel = assembler.toModel(Skill);

        return ResponseEntity //
                .created(entityModel.getRequiredLink(IanaLinkRelations.SELF).toUri()) //
                .body(entityModel);
    }

    @PostMapping("/skills/addContact/{id}")
    @ApiOperation(value = "Adds a contact from a skill from the database by id and contact")
    ResponseEntity<?> addContact(@PathVariable Long id, @RequestBody Contact contact) {

        Skill skill = skillRepository.findById(id).orElseThrow(() -> new SkillNotFoundException(id));
        Contact contact1 = new Contact(
                contact.getFirstName(),
                contact.getLastName(),
                contact.getAddress(),
                contact.getEmail(),
                contact.getPhoneNumber()
                );
        skill.getContact().add(contact1);
        skillRepository.save(skill);
        EntityModel<Skill> entityModel = assembler.toModel(skill);
        return ResponseEntity //
                .created(entityModel.getRequiredLink(IanaLinkRelations.SELF).toUri()) //
                .body(entityModel);
    }
}
