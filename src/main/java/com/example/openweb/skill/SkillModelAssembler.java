package com.example.openweb.skill;

import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.*;

import org.springframework.hateoas.EntityModel;
import org.springframework.hateoas.server.RepresentationModelAssembler;
import org.springframework.stereotype.Component;

@Component
class SkillModelAssembler implements RepresentationModelAssembler<Skill, EntityModel<Skill>> {

    @Override
    public EntityModel<Skill> toModel(Skill skill) {

        // Unconditional links to single-item resource and aggregate root
        return EntityModel.of(skill,
                linkTo(methodOn(SkillController.class).one(skill.getId())).withSelfRel(),
                linkTo(methodOn(SkillController.class).all()).withRel("skills"));
    }
}
