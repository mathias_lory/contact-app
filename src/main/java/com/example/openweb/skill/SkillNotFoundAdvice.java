package com.example.openweb.skill;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

import java.util.HashMap;
import java.util.Map;

@ControllerAdvice
class SkillNotFoundAdvice {

    @ResponseBody
    @ExceptionHandler(SkillNotFoundException.class)
    @ResponseStatus(HttpStatus.NOT_FOUND)
    String skillNotFoundHandler(SkillNotFoundException ex) {
        return ex.getMessage();
    }

    @org.springframework.web.bind.annotation.ExceptionHandler
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    @ResponseBody
    Map<String,String> showCustomMessage(Exception e){
        Map<String,String> response = new HashMap<>();
        response.put("BAD REQUEST","The level must be one of the following : FUNDAMENTAL, NOVICE, INTERMEDIATE, ADVANCED or EXPERT " + e);
        return response;
    }
}