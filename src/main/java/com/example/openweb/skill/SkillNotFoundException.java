package com.example.openweb.skill;

class SkillNotFoundException extends RuntimeException {

    SkillNotFoundException(Long id) {
        super("Could not find skill " + id);
    }
}