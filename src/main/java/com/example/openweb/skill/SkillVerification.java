package com.example.openweb.skill;

import com.example.openweb.util.Verification;

import java.util.HashMap;
import java.util.Map;

public class SkillVerification extends Verification {

    public Map<Boolean, String> checkContact(Skill newSkill) {
        Map<Boolean, String> responseMap = new HashMap<>();
        checkAlphaNum(newSkill.getName(), "name", responseMap);
        return responseMap;
    }

}
