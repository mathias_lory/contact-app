package com.example.openweb.util;

import com.example.openweb.skill.Level;
import com.example.openweb.skill.Skill;
import com.example.openweb.skill.SkillRepository;
import com.example.openweb.contact.Contact;
import com.example.openweb.contact.ContactRepository;
import com.example.openweb.user.User;
import com.example.openweb.user.UserRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.CommandLineRunner;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
class LoadDatabase {

    private static final Logger log = LoggerFactory.getLogger(LoadDatabase.class);

    @Bean
    CommandLineRunner initDatabase(ContactRepository contactRepository, SkillRepository skillRepository, UserRepository userRepository) {
        return args -> {
            log.info("Preloading " + contactRepository.save(new Contact("Michel", "Dupont", "1 rue de noailles", "michel.dupont@gmail.com", "0786560801")));
            log.info("Preloading " + contactRepository.save(new Contact("Marie", "Dubois", "39 bis rue robert poisson", "marie.dubois@gmail.com", "0786560802")));
            log.info("Preloading " + contactRepository.save(new Contact("Sam", "Dufour", "42 rue de grinloup", "sam.dufour@gmail.com", "0786560803")));
            log.info("Preloading " + contactRepository.save(new Contact("Teddy", "Duprès", "20 rue montgolfier", "teddy.dupres@gmail.com", "0786560804")));
            log.info("Preloading " + contactRepository.save(new Contact("Angie", "Durant", "7 allée gaston", "angie.durant@gmail.com", "0786560805")));
            log.info("Preloading " + contactRepository.save(new Contact("Mylene", "Dubuisson", "29 avenue auguste dutreux", "mylene.dubuisson@gmail.com", "0786560806")));
            log.info("Preloading " + contactRepository.save(new Contact("Suzanne", "Duvent", "3 rue de l'église", "suzanne.duvent@gmail.com", "0786560807")));
            log.info("Preloading " + contactRepository.save(new Contact("Gilbert", "Duval", "15 rue d'albret", "gilbert.duval@gmail.com", "0786560808")));
            log.info("Preloading " + contactRepository.save(new Contact("Nicole", "Dupuit", "116 rue jean zay", "nicole.dupuit@gmail.com", "0786560809")));
            log.info("Preloading " + contactRepository.save(new Contact("Pierrette", "Dufrais", "12 rue jeanne d'arc", "pierrette.dufrais@gmail.com", "0786560810")));
            log.info("Preloading " + contactRepository.save(new Contact("François", "Dumont", "16 avenue de la petite école", "francois.dumont@gmail.com", "0786560811")));
            log.info("Preloading " + contactRepository.save(new Contact("François", "Dumont", "26 boulevard de la corniche", "francoisdumont@gmail.com", "0786560812")));
            log.info("Preloading " + skillRepository.save(new Skill("Hibernate", Level.INTERMEDIATE)));
            log.info("Preloading " + skillRepository.save(new Skill("Java", Level.EXPERT)));

            // Connected Skills and Contacts
            Contact contact = new Contact("Bruce", "Dupuissant", "5 rue de l'ascension", "bruce@gmail.com", "0786560815");
            Contact contact2 = new Contact("Hubert", "Dutagne", "26 rue du ski", "hubert.dutagne@gmail.com", "0786560812");
            Skill skill1 = new Skill("Spring Boot", Level.INTERMEDIATE);
            Skill skill2 = new Skill("Hibernate", Level.ADVANCED);
            contact.getskill().add(skill1);
            contact.getskill().add(skill2);
            skill1.getContact().add(contact);
            skill2.getContact().add(contact);
            contactRepository.save(contact);
            contactRepository.save(contact2);
            User user = new User();
            user.setUsername("ADMIN");
            user.setPassword("ADMIN");
            userRepository.save(user);
        };
    }
}