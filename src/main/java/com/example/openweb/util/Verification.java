package com.example.openweb.util;

import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import java.util.Map;

public class Verification {

    protected void checkOnlyChar(String stringToCheck, String fieldName, Map<Boolean, String> responseMap) {
        if (stringToCheck == null || !stringToCheck.matches("[a-zA-Z]+")) {
            responseMap.put(true, "You can't create a contact with special characters or numbers in its " + fieldName);
        }
    }

    protected void checkOnlyNum(String stringToCheck, Map<Boolean, String> responseMap) {
        if (stringToCheck == null || !stringToCheck.matches("[0-9]{10}")) {
            responseMap.put(true, "The phone number must contain exactly 10 digits ");
        }
    }

    protected void checkMail(String email, Map<Boolean, String> responseMap) {
        try {
            InternetAddress emailAddr = new InternetAddress(email);
            emailAddr.validate();
        } catch (AddressException ex) {
            responseMap.put(true, "The email address must be correct");
        }
    }

    protected void checkAlphaNum(String address, String fieldName,Map<Boolean, String> responseMap) {
        address = address.replace(" ", "");
        if (!address.matches("(\\d*\\w*)")) {
            responseMap.put(true, "The " + fieldName + " must only contain letters and numbers");
        }
    }
}
