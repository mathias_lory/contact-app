package com.example.openweb;

import com.example.openweb.contact.Contact;
import com.example.openweb.contact.ContactRepository;
import com.example.openweb.skill.Level;
import com.example.openweb.skill.Skill;
import com.example.openweb.skill.SkillRepository;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
class OpenwebApplicationTests {

	@Autowired
	private ContactRepository contactRepository;

	@Autowired
	private SkillRepository skillRepository;

	@Test
	void contextLoads() {

		// Create a contact
		Contact contact = new Contact("François", "Dupuit", "26 boulevard du test", "francoisdupuit@gmail.com", "0786560812");
		Contact contact2 = new Contact("Hubert", "DelaBatte", "26 rue du canard", "noel.flantier@gmail.com", "0786560812");

		// Create two skills
		Skill skill1 = new Skill("Spring Boot", Level.INTERMEDIATE);
		Skill skill2 = new Skill("Hibernate", Level.ADVANCED);


		// Add tag references in the post
		contact.getskill().add(skill1);
		contact.getskill().add(skill2);

		// Add post reference in the tags
		skill1.getContact().add(contact);
		skill2.getContact().add(contact);

		contactRepository.save(contact);
		contactRepository.save(contact2);
	}

}
